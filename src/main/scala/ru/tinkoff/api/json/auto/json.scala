package ru.tinkoff.api.json.auto

import java.time.LocalDate


case class Bank(name: String,
                bic: String,
                address: String,
                corrAccount: String)

case class BankAccount(accountNumber: String,
                       bank: Bank)

case class Transaction(source: BankAccount, target: BankAccount, sum: BigDecimal, date: LocalDate)




object AutoDeviation extends App {
  import io.circe.generic.auto._, io.circe.syntax._

  val bankAccount = BankAccount(
    accountNumber = "143125915414123",
    bank = Bank(
      "some bank",
      "125891859018412",
      "Harlem",
      "83590789573489593"
    )
  )

  val json = bankAccount.asJson
  println(json)

}