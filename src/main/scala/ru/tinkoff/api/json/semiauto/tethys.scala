package ru.tinkoff.api.json.semiauto.tethysjson

import tethys._
import tethys.jackson._
import derivation.semiauto._
import tethys.derivation.builder._

case class Foo(foo: String, bar: Int, kek: BigDecimal)

object Foo {

  implicit val reader: JsonReader[Foo] = jsonReader
  implicit val writer: JsonObjectWriter[Foo] = jsonWriter
}

object HiddenField extends App {
  val foo = Foo("foo", 100, 1000)

  val full = foo.asJson // {"foo":"foo","bar":100,"kek":1000}

  val hidden = {
    implicit val hiddenWriter: JsonObjectWriter[Foo] = jsonWriter[Foo] {
      describe {
        WriterBuilder[Foo]
          .remove(_.kek)
      }
    }
    foo.asJson
  }

  println(hidden) // {"foo":"foo","bar":100,}




}