package ru.tinkoff.api.json.semiauto

import java.time.LocalDate

import io.circe.Encoder
import io.circe.generic.semiauto._


case class Bank(name: String,
                bic: String,
                address: String,
                corrAccount: String)

object Bank {
  implicit val encoder: Encoder[Bank] = deriveEncoder
}

case class BankAccount(accountNumber: String,
                       bank: Bank)

object BankAccount {
  implicit val encoder: Encoder[BankAccount] = deriveEncoder
}

case class Transaction(source: BankAccount, target: BankAccount, sum: BigDecimal, date: LocalDate)

object Transaction {
  implicit val encoder: Encoder[Transaction] = deriveEncoder
}



object SemiautoDeviation extends App {
  import io.circe.syntax._

  val sourceBankAccount = BankAccount(
    accountNumber = "143125915414123",
    bank = Bank(
      "some bank",
      "125891859018412",
      "Harlem",
      "83590789573489593"
    )
  )

  val targetBankAccount = BankAccount(
    accountNumber = "984509283502",
    bank = Bank(
      "some bank",
      "125891859018412",
      "Harlem",
      "83590789573489593"
    )
  )

  val transaction = Transaction(sourceBankAccount, targetBankAccount, 100, LocalDate.now())

  val json = transaction.asJson

  println(json)

}