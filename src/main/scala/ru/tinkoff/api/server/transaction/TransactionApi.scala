package ru.tinkoff.api.server.transaction

import java.time.LocalDate

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.{Directives, Route}
import io.circe.{Decoder, Encoder}

import scala.concurrent.{ExecutionContext, Future}

case class Transaction(sum: BigDecimal, date: LocalDate, bankAccount: String)

object Transaction {

  import io.circe.generic.semiauto._

  implicit val encoder: Encoder[Transaction] = deriveEncoder
  implicit val decoder: Decoder[Transaction] = deriveDecoder
}

class TransactionService {
  def get(transactionId: Int): Future[Transaction] =
    Future.successful {
      Transaction(100, LocalDate.now(), "1231231231209")
    }

  def list(): Future[Seq[Transaction]] = Future.successful {
    Seq(
      Transaction(100, LocalDate.now(), "1231231231209"),
      Transaction(1, LocalDate.now(), "1231231231209"),
      Transaction(2, LocalDate.now(), "1231231231209")
    )
  }

  def save(transaction: Transaction): Future[Transaction] = Future.successful(transaction)

}


class TransactionApi(transactionService: TransactionService) extends Directives {
  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  val route: Route = Route.seal {
    pathPrefix("api" / "v1") {
      pathPrefix("transaction") {
        pathEnd {
          get {
            complete(transactionService.list())
          } ~ (post & entity(as[Transaction])) { transaction =>
            complete(transactionService.save(transaction))
          }
        } ~
          path(Segment) { transactionId =>
            get {
              complete(transactionService.get(transactionId.toInt))
            }
          }
      }
    }
  }
}


object TransactionApp extends App {

  import ExecutionContext.Implicits.global

  implicit val actorSystem: ActorSystem = ActorSystem()

  val service = new TransactionService()
  val api = new TransactionApi(service)

  val handler = Http().bindAndHandle(api.route, "localhost", 8080)

}