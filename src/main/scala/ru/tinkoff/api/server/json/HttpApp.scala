package ru.tinkoff.api.server.json

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route


import scala.concurrent.{ExecutionContext, Future}
import scala.util.Failure

case class HttpApp(route: Route) {
  def run()(implicit ec: ExecutionContext): Future[Http.ServerBinding] = {
    implicit val actorSystem: ActorSystem = ActorSystem()

    val result: Future[Http.ServerBinding] = Http().bindAndHandle(route, "localhost", 8080)

    result
      .onComplete {
        case Failure(exception) =>
          exception.printStackTrace()
          actorSystem.terminate()
        case _ =>
      }

    result
  }
}
