package ru.tinkoff.api.server.json

import akka.http.scaladsl.server.Directives
import ru.tinkoff.api.json.semiauto.Transaction
import ru.tinkoff.api.server.domain.example
import io.circe.syntax._
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import scala.concurrent.ExecutionContext.Implicits.global


import scala.concurrent.Future

object TransactionService {
  def listTransactions(): Future[Seq[Transaction]] =
    Future.successful(Seq(example.transaction))
}

object TransactionApi extends Directives{
  val route =
    path("api" / "v1" / "transaction") {
      get {
        onSuccess(TransactionService.listTransactions()) { transactions =>
          complete(transactions.asJson)
        }
      }
    }
}


object JsonApiApp extends App {
  HttpApp(TransactionApi.route).run()
}

