package ru.tinkoff.api.server.simple

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Failure
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._


object SimpleServer extends App {

  implicit val actorSystem: ActorSystem = ActorSystem()


  val routes: Route = {
    import akka.http.scaladsl.server.Directives._

    val kek = path("kek") {
      get {
        parameter("name".as[String]) { name =>
          complete(s"lol $name")
        }
      }
    }

    val hello =
      path("hello") {
        get {
          parameter("name".as[String]) { name =>
            complete(s"Hello $name")
          }
        }
      }
    kek ~ hello
  }

  val result: Future[Http.ServerBinding] = Http().bindAndHandle(routes, "localhost", 8080)

  result
    .onComplete {
      case Failure(exception) =>
        exception.printStackTrace()
        actorSystem.terminate()
      case _ =>
    }
}