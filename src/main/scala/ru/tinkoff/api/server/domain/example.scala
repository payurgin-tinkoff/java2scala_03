package ru.tinkoff.api.server.domain

import java.time.LocalDate

import ru.tinkoff.api.json.semiauto.{Bank, BankAccount, Transaction}

object example {
  val sourceBankAccount = BankAccount(
    accountNumber = "143125915414123",
    bank = Bank(
      "some bank",
      "125891859018412",
      "Harlem",
      "83590789573489593"
    )
  )

  val targetBankAccount = BankAccount(
    accountNumber = "984509283502",
    bank = Bank(
      "some bank",
      "125891859018412",
      "Harlem",
      "83590789573489593"
    )
  )

  val transaction = Transaction(sourceBankAccount, targetBankAccount, 100, LocalDate.now())
}
