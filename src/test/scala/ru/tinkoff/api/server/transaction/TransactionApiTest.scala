package ru.tinkoff.api.server.transaction

import java.time.LocalDate

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import io.circe.{Json, parser}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.Future

class TransactionApiTest extends FlatSpec
  with Matchers
  with MockFactory
  with ScalatestRouteTest {

  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  "GET /api/v1/transaction" should "return list of transactions" in {

    (mockTransactionService.list _)
        .expects()
        .returns(Future.successful(Seq(sampleTransaction)))

    Get("/api/v1/transaction") ~> transactionApi.route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Json] shouldBe parser.parse(
        """
          |[
          |  {
          |    "sum" : 100,
          |    "date" : "2020-03-17",
          |    "bankAccount" : "1231231231209"
          |  }
          |]""".stripMargin
      ).right.get
    }
  }

  "GET /api/v1/transaction/{id}" should "return transaction" in {

    (mockTransactionService.get _)
      .expects(100)
      .returns(Future.successful(sampleTransaction))

    Get("/api/v1/transaction/100") ~> transactionApi.route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Json] shouldBe parser.parse(
        """
          |{
          |  "sum" : 100,
          |  "date" : "2020-03-17",
          |  "bankAccount" : "1231231231209"
          |}
          |""".stripMargin
      ).right.get
    }
  }

  "POST /api/v1/transactions/{id}" should "return empty object" in {
    (mockTransactionService.save _)
      .expects(sampleTransaction)
      .returns(Future.successful(sampleTransaction))

    Post("/api/v1/transaction", sampleTransaction) ~> transactionApi.route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Json] shouldBe parser.parse(
        """
          |{
          |  "sum" : 100,
          |  "date" : "2020-03-17",
          |  "bankAccount" : "1231231231209"
          |}
          |""".stripMargin
      ).right.get
    }
  }

  private val sampleTransaction = Transaction(100, LocalDate.now(), "1231231231209")
  private val mockTransactionService = mock[TransactionService]
  private val transactionApi = new TransactionApi(mockTransactionService)
}
